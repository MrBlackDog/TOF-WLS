function [H] = H_TOF_2d(X,SatPos)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

for k=1:size(SatPos,2)
    norm = sqrt((X(1,1)-SatPos(1,k))^2 + (X(2,1)-SatPos(2,k))^2);
    if(~norm==0)
        H(k,1) = (X(1,1) - SatPos(1,k))/norm;
        H(k,2) = (X(2,1) - SatPos(2,k))/norm;
    else
        H(k,:) = [0;0];
    end
    
end
