function [F] = F_TOF_2d(X,SatPos)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
for k=1:size(SatPos,2)
 F(k,1) = sqrt((X(1,1)-SatPos(1,k))^2 + (X(2,1)-SatPos(2,k))^2);
end

