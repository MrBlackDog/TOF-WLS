function [x] = TOF_WLS_2d(R,SatPos)

x = zeros(2,size(R,2));
for j=1:size(R,2)
    x1(:,1) = [2;2];
    for i=2:10
        H = H_TOF_2d(x(:,i-1),SatPos);
        x1(:,i)=x1(:,i-1) + (H'*H)^-1 * H'*(R(:,j)-F_TOF_2d(x(:,i-1),SatPos));
    end
    x(:,j) = x1(:,end);
end

end